# Security Topics


This repository intends to provide some tools to support the classification of information against security topics. 

## CyBoK

The [CyBoK](https://www.cybok.org) is an effort to define a "comprehensive Body of Knowledge to inform and underpin education and professional training for the cyber security sector." 

Source for the topics: https://www.cybok.org/media/downloads/CyBOK_Knowledge_trees_topic_list_1.1.0.csv

Note: we also provide a slightly curated version, to help with topic classification. 
